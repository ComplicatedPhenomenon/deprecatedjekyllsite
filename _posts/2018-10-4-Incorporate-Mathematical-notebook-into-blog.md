---
layout: post
title:  "Incorporate Mathematica Notebook into blog"
date:   2018-10-4 10:10:10 +0700
categories: [jekyll, Mathematica Notebook]
comments: true
---
# [How to export a Mathematica notebook into Markdown?](https://mathematica.stackexchange.com/questions/84556/how-to-export-a-mathematica-notebook-into-markdown)

<iframe width="560" height="315" src="https://www.youtube.com/embed/ghwaIiE3Nd8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
